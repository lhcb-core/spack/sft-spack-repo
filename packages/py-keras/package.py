# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyKeras(PythonPackage):
    """TensorFlow Keras is an implementation of the
    Keras API that uses TensorFlow as a backend."""

    homepage = "https://keras.io/"
    url      = "https://files.pythonhosted.org/packages/4f/2f/eb9391bdcba2693cc8396f244bd3b4512bcd1123c2ea06f4dfcf50dc5ce9/keras-2.8.0-py2.py3-none-any.whl"

    version('2.8.0', sha256='744d39dc6577dcd80ff4a4d41549e92b77d6a17e0edd58a431d30656e29bc94e', expand=False)

    depends_on('py-setuptools', type='build')
    depends_on('py-pandas', type=('build', 'run'))
    depends_on('py-pydot', type=('build', 'run'))
    depends_on('py-scipy@1.5.2:', type=('build', 'run'))
    depends_on('py-pyyaml', type=('build', 'run'))
    depends_on('pil', type=('build', 'run'))
    depends_on('py-numpy@1.19.2:', type=('build', 'run'))
