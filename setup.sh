#!/bin/bash
echo Cloning spack...
git clone https://github.com/spack/spack.git
echo Cloning SFT repo...
git clone https://:@gitlab.cern.ch:8443/sft/sft-spack-repo.git -b LCG_98 spack/var/spack/repos/spi
echo Prioritizing SFT recipes
cp spack/var/spack/repos/spi/config/repos.yaml spack/etc/spack/repos.yaml
echo Setting up system packages and default variants
cp spack/var/spack/repos/spi/config/packages.yaml spack/etc/spack/packages.yaml
echo Adding compiler
mkdir -p spack/etc/spack/linux
cp spack/var/spack/repos/spi/config/compilers.yaml spack/etc/spack/linux/compilers.yaml
echo Adding SFT mirror
cp spack/var/spack/repos/spi/config/mirrors.yaml spack/etc/spack/linux/mirrors.yaml
echo Adding environment
cp -r spack/var/spack/repos/spi/config/LCG_98python3_ATLAS_5 spack/var/spack/environments/
# docker run -it -v /cvmfs/sft.cern.ch:/cvmfs/sft.cern.ch -v /build/razumov/spack:/workspace gitlab-registry.cern.ch/sft/docker/centos7
# mkdir -p /cvmfs/sw.hsf.org/sft-spack