from datetime import datetime
import os
import platform
try:
    #TODO: can be removed when spack versions prior to v0.18.1 are no longer needed.
    from spack.package import PackageBase
except ImportError:
    from spack.package_base import PackageBase

import llnl.util.tty as tty
import spack.spec
import spack.platforms
from spack.main import get_version
import spack.user_environment as uenv
from  spack.util.environment import *

class Setupscriptpackage(Package):
    pass

class SetupScriptPackage(PackageBase):
    """Convenience methods to create a setup script on installation"""
    

    # this bundle package installs a custom setup script, so
    # need to add the install phase (which normally does not
    # exist for a bundle package)
    phases = ['install']

    def k4_generate_setup_script(self, env_mod, shell='sh'):
        """Return shell code corresponding to a EnvironmentModifications object.
        Contrary to the spack environment_modifications() method, this does not evaluate
        the current environment, but generates shell code like:
        export PATH=/new/path:$PATH
        instead of:
        export PATH=/new/path:/current/contents/of/PATH;
        if `/new/path` is to be prepended.

        :param env_mod: spack EnvironmentModifications object
        :type env_mod: class: `spack.EnvironmentModifications`
        :param str shell: type of the shell. Only 'sh' possible at the moment
        :return: Shell code corresponding to the environment modifications.
        :rtype: str
        """
        modifications = env_mod.group_by_name()
        new_env = {}
        # keep track wether this variable is supposed to be a list of paths, or set to a single value
        env_set_not_prepend = {} 
        for name, actions in sorted(modifications.items()):
            env_set_not_prepend[name] = False
            for x in actions:
                env_set_not_prepend[name] = env_set_not_prepend[name] or isinstance(x, (SetPath, SetEnv))
                # set a dictionary with the environment variables
                x.execute(new_env)
            if env_set_not_prepend[name] and len(actions) > 1:
                tty.warn("Var " + name + "is set multiple times!" )
      
        # deduplicate paths
        for name in  new_env:
          path_list = new_env[name].split(":")
          pruned_path_list = prune_duplicate_paths(path_list)
          new_env[name] = ":".join(pruned_path_list) 


        # fourth, get shell commands
        k4_shell_set_strings = {
            'sh': 'export {0}={1};\n',
            'csh': 'setenv {0} {1};\n',
            'fish': 'set -gx {0} {1};\n'

        }
        k4_shell_prepend_strings = {
            'sh': 'export {0}={1}:${0};\n',
            'csh': 'setenv {0} {1}:${0};\n',
            'fish': 'set -gx {0} {1}:${0};\n'
        }
        cmds = []
        for name in set(new_env):
            if env_set_not_prepend[name]:
                cmds += [k4_shell_set_strings[shell].format(
                    name, cmd_quote(new_env[name]))]
            else:
                cmds += [k4_shell_prepend_strings[shell].format(
                    name, cmd_quote(new_env[name]))]
        return ''.join(cmds)

    def k4_lookup_latest_commit(repoinfo, giturl):
        """Use a github-like api to fetch the commit hash of the master branch.
        Constructs and runs a command of the form:
        # curl -s -u user:usertoken https://api.github.com/repos/hep-fcc/fccsw/commits/master -H "Accept: application/vnd.github.VERSION.sha"
        The authentication is optional, but note that the api might be rate-limited quite strictly for unauthenticated access.
        The envrionment variables 
          GITHUB_USER
          GITHUB_TOKEN
        can be used for authentication.

        :param repoinfo: description of the owner and repository names, p.ex: "key4hep/edm4hep"
        :type repoinfo: str
        :param giturl: url that will return a json response with the commit sha when queried with urllib.
           should contain a %s which will be substituted by repoinfo.
           p.ex.: "https://api.github.com/repos/%s/commits/master"
        :return: The commit sha of the latest commit for the repo.
        :rtype: str
          
        """
        curl_command = ["curl -s "]
        github_user = os.environ.get("GITHUB_USER", "")
        github_token = os.environ.get("GITHUB_TOKEN", "")
        if github_user and github_token:
          curl_command += [" -u %s:%s " % (github_user, github_token)]
        final_giturl = giturl % repoinfo
        curl_command += [final_giturl]
        curl_command += [' -H "Accept: application/vnd.github.VERSION.sha" ']
        curl_command = ' '.join(curl_command)
        commit = os.popen(curl_command).read()
        test = int(commit, 16)
        return commit

    def k4_add_latest_commit_as_dependency(name, repoinfo, giturl="https://api.github.com/repos/%s/commits/master", variants="", when="@master"):
        """ Helper function that adds a 'depends_on' with the latest commit to a spack recipe.

        :param name: spack name of the package, p.ex: "edm4hep"
        :type name: str
        :param repoinfo: description of the owner and repository names, p.ex: "key4hep/edm4hep"
        :type repoinfo: str
        :param giturl: url that will return a json response with the commit sha when queried with urllib.
           should contain a %s which will be substituted by repoinfo.
           p.ex.: "https://api.github.com/repos/%s/commits/master"
        :type giturl:, str, optional
        :param variants: argument that will be forwarded to depends_on
          example: "+lcio"
        :type variants: str, optional
        :param when: argument that will be forwarded to depends_on
          example: "@master"
        :type when: str, optional
        """
        github_user = os.environ.get("GITHUB_USER", "")
        github_token = os.environ.get("GITHUB_TOKEN", "")
        if github_user and github_token:
          try:
            commit = k4_lookup_latest_commit(repoinfo, giturl)
            depends_on(name + "@commit." + str(commit) + " " + variants, when=when)
          except:
            print("Warning: could not fetch latest commit for " + name)

    def k4_add_latest_commit_as_version(git_url, git_api_url="https://api.github.com/repos/%s/commits/master"):
        """ Helper function that adds a 'version' with the latest commit to a spack recipe.
        Note that the 'commit' part of the version is needed to ensure that version comparisons in spack will judge this as the newest version.


        :param git_url: url of a git repository. Needs to end in .git.
          example: "https://github.com/HSF/prmon.git"
        :type git_url: str
        :param giturl: url that will return a json response with the commit sha when queried with urllib.
           should contain a %s which will be substituted by repoinfo.
           p.ex.: "https://api.github.com/repos/%s/commits/master"
        :type giturl: str, optional
        """
        pass

    def install(self, spec, prefix):
      """ Create bash setup script in prefix."""
      # first, log spack version to build-out
      tty.msg('* **Spack:**', get_version())
      tty.msg('* **Python:**', platform.python_version())
      tty.msg('* **Platform:**', spack.platforms.host())
      # get all dependency specs, including compiler
      with spack.store.db.read_transaction():
               specs = [dep for dep in spec.traverse(order='post')]
      # record all changes to the environment by packages in the stack
      env_mod = spack.util.environment.EnvironmentModifications()
      # first setup compiler, similar to build_environment.py in spack
      compiler = self.compiler
      if compiler.cc:
          env_mod.set('CC', compiler.cc)
      if compiler.cxx:
          env_mod.set('CXX', compiler.cxx)
      if compiler.f77:
          env_mod.set('F77', compiler.f77)
      if compiler.fc:
          env_mod.set('FC',  compiler.fc)
      compiler.setup_custom_environment(self, env_mod)
      env_mod.prepend_path('PATH', os.path.dirname(compiler.cxx))
      # now setup all other packages
      for _spec in specs:
          env_mod.extend(uenv.environment_modifications_for_spec(_spec))
          env_mod.prepend_path(uenv.spack_loaded_hashes_var, _spec.dag_hash())
      # transform to bash commands, and write to file
      os.makedirs(os.path.join(prefix, "share", self.name))
      cmds_csh = self.k4_generate_setup_script(env_mod, shell="csh")
      with open(os.path.join(prefix, "share", self.name, "setup.csh"), "w") as f:
        f.write(cmds_csh)
      cmds = self.k4_generate_setup_script(env_mod, shell="sh")
      with open(os.path.join(prefix, "share", self.name, "setup.sh"), "w") as f:
        f.write(cmds)
        # optionally add a symlink (location configurable via environment variable
        # K4_LATEST_SETUP_PATH. Step will be skipped if it is empty)
        try:
          symlink_path = os.environ.get("K4_LATEST_SETUP_PATH", "")
          if symlink_path:
              # make sure that the path exists, create if not
              if not os.path.exists(os.path.dirname(symlink_path)):
                os.makedirs(os.path.dirname(symlink_path))
              # make sure that an existing file will be overwritten,
              # even if it is a symlink (for which 'exists' is false!)
              if os.path.exists(symlink_path) or os.path.islink(symlink_path):
                os.remove(symlink_path)
              os.symlink(os.path.join(prefix, "setup.sh"), symlink_path)
        except:
          tty.warn("Could not create symlink")
