# Copyright 2013-2022 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class PyTensorflowEstimator(PythonPackage):
    """TensorFlow Estimator is a high-level API that encapsulates
    model training, evaluation, prediction, and exporting."""

    homepage = "https://github.com/tensorflow/estimator"
    url      = "https://files.pythonhosted.org/packages/4e/42/cefc351e0e428ca9c460bfbd994ec9f01e5d8bd34f88c65a7ea807ecd67a/tensorflow_estimator-2.8.0-py2.py3-none-any.whl"

    version('2.8.0', sha256='bee8e0520c60ae7eaf6ca8cb46c5a9f4b45725531380db8fbe38fcb48478b6bb', expand=False)

    depends_on('py-setuptools', type='build')
    depends_on('py-tensorflow@2.8.0:2.8', type=('build', 'run'), when='2.8.0')
    depends_on('py-funcsigs@1.0.2:', type=('build', 'run'))

    extends('python')
